# README #

### What is this application for? ###

This application is a storefront cartridge that you can test OCAPI SHOP queries on your browser.

### How do I get set up? ###

* How to set up
    1. Download the cartridge.
    2. Import it to your DW Studio and upload it to your sandbox.
    3. Add "app_ocapi_test" to the cartridge path of your site.
    4. Access https://<your-instance-domain>/on/demandware.store/Sites-<your-site>-Site/default/OCAPITest-Start
* Configuration
    1. Set proper OCAPI configuration on Business Manager.
* Dependencies
    * It will use "content/home/pt_storefront.isml" template.
* Recommended brwoser
    * Chrome

### Features ###
* SHOP API calls with JSON formatted data.
* GET/POST/PUT/DELETE/PATCH method supported.
* URL parameter supported.
* JSON validation / formatter available.
* Additional HTTP request header supported.
* HTTP body supported.
* If ETag is returned in the response header, it will be saved as If-Match header parameter (you need to use "Back" link at the bottom of the result page on the app).

### What you cannot do ###
* DATA API calls
* XML format request

### Who do I talk to? ###

* Repo owner (himamura@demandware.com)

### Screenshot ###
Top Page
![top.png](https://bitbucket.org/repo/Mog9ke/images/189429907-top.png)
'use strict';

/**
 * Controller that executes OCAPI calls and displays the results.
 *
 * @module controllers/OCAPITest
 */
var ISML = require('dw/template/ISML');
var URLUtils = require('dw/web/URLUtils');
var HTTPClient = require('dw/net/HTTPClient');
var System = require('dw/system/System');
var Map = require('dw/util/Map');
var HashMap = require('dw/util/HashMap');
var StringUtils = require('dw/util/StringUtils');
var guard = require('app_ocapi_test/cartridge/scripts/guard');

function start()
{
	var hostname = System.instanceHostname;
	var currentHttpParameterMap = request.httpParameterMap
	var etag = currentHttpParameterMap.etag.stringValue;
	
	ISML.renderTemplate('ocapitest.isml', {
		ContinueURL : URLUtils.url('OCAPITest-Call'),
		CurrentForms : session.forms,
		HostName : hostname,
		ETag : etag
	});
}

function call()
{
	var triggeredAction = request.triggeredFormAction;
	if(triggeredAction != null)
	{
		if(triggeredAction.formId == 'submit')
		{
			var ocapitestForm = session.forms.ocapitest;
			var domain = ocapitestForm.domain.value;
			var site = ocapitestForm.site.value;
			var version = ocapitestForm.version.value;
			var clientid = ocapitestForm.clientid.value;
			var resource = ocapitestForm.resource.value;
			var param1 = ocapitestForm.param1.value;
			var param2 = ocapitestForm.param2.value;
			var param3 = ocapitestForm.param3.value;
			var protocol = ocapitestForm.protocol.value;
			var type = ocapitestForm.type.value;
			var text = ocapitestForm.text.value;
			var isPatch = false;
			if(text != null)
			{
				text = StringUtils.trim(text);
			}
			var requestHeadersText = ocapitestForm.requestheaders.value;
			var requestHeadersMap : Map;
			
			if(requestHeadersText != null && requestHeadersText != '')
			{
				requestHeadersMap = parseRequestHeaderText(requestHeadersText);
			}
			
			var ocapiUrl = protocol + '://' + domain + '/s/' + site + '/dw/shop/' + version + '/' + resource + '?' + 'client_id=' + clientid;
			
			if(param1 != null)
			{
				ocapiUrl += '&' + param1;
			}
			if(param2 != null)
			{
				ocapiUrl += '&' + param2;
			}
			if(param3 != null)
			{
				ocapiUrl += '&' + param3;
			}
			
			var httpClient = new HTTPClient();
			httpClient.setTimeout(10000);
			var message : String;
			var headers : Map;
			if(type == 'GET')
			{
				try
				{
					httpClient.open(type, ocapiUrl);
					addRequestHeaders(httpClient, requestHeadersMap);
					httpClient.send();
					if(httpClient.statusCode >= 200 && httpClient.statusCode <= 299)
					{
						message = httpClient.text;
					}
					else
					{
						message = httpClient.errorText;
					}
					headers = httpClient.responseHeaders;
				}
				catch(e)
				{
					message = e.name + ': ' + e.message;
				}
			}
			else if(type == 'POST' || type == 'PUT' || type == 'DELETE' || type == 'PATCH')
			{
				try
				{
					if(type == 'PATCH')
					{
						// Overriding HTTP method
						type = 'POST';
						ocapiUrl += '&method=PATCH';
						isPatch = true;
					}
					httpClient.open(type, ocapiUrl);
					addRequestHeaders(httpClient, requestHeadersMap);
					httpClient.send(text, 'UTF-8');
					if(httpClient.statusCode >= 200 && httpClient.statusCode <= 299)
					{
						message = httpClient.text;
					}
					else
					{
						message = httpClient.errorText;
					}
					headers = httpClient.responseHeaders;
				}
				catch(e)
				{
					message = e.name + ': ' + e.message;
				}
			}
			else
			{
				message = 'Invalid method: ' + type;
			}
			
			if(isPatch)
			{
				type = 'PATCH';
			}
			
			ISML.renderTemplate('ocapiresponse.isml', {
				OCAPIURL : ocapiUrl,
				HttpRequestHeaders : requestHeadersMap,
				HTTPResponseCode : httpClient.statusCode,
				HTTPResponseMessage : message,
				HTTPResponseHeaders : headers,
				type : type,
				text : text
			});
		}
		else
		{
			ISML.renderTemplate('invalidrequest.isml');
		}
	}
	else
	{
		ISML.renderTemplate('invalidrequest.isml');
	}
}

function parseRequestHeaderText(text : String) : Map
{
	var regexp1 = new RegExp('\r\n');
	var regexp2 = new RegExp('\r\g');
	text = text.replace(regexp1,'\n');
	text = text.replace(regexp2, '\n');
	var headerList = text.split('\n');
	
	var regexp3 : RegExp = new RegExp('^(.*):\s?(.*)$');
	var headers : HashMap = new HashMap();
	
	headerList.forEach(function(line) {
		line = StringUtils.trim(line);
		if(line != '')
		{
			var arr = regexp3.exec(line);
			if(arr.length == 3)
			{
				headers.put(arr[1], arr[2]);
			}
		}
	});
	
	return headers;
}

function addRequestHeaders(httpClient : HTTPClient, requestHeadersMap : Map)
{
	if(requestHeadersMap != null)
	{
		var keys = requestHeadersMap.keySet().iterator();
		while(keys.hasNext())
		{
			var key = keys.next();
			httpClient.setRequestHeader(key, requestHeadersMap.get(key));
		}
	}
	httpClient.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
}

exports.Start = guard.ensure(['https', 'get'], start);
exports.Call = guard.ensure(['https', 'post'], call);